#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve


# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self): # given
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_1(self): # written
        s = "2 3\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2)
        self.assertEqual(j, 3)

    def test_read_2(self): # written
        s = "1 9999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 9999)

    def test_read_3(self): # written
        s = "9998 9999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  9998)
        self.assertEqual(j, 9999)

    def test_read_4(self): # written
        s = "333 666\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  333)
        self.assertEqual(j, 666)

    # ----
    # eval
    # ----

    def test_eval_1(self): # given
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self): # given
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self): # given
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self): # given 
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self): # written
        v = collatz_eval(2, 3)
        self.assertEqual(v, 8)

    def test_eval_6(self): # written
        v = collatz_eval(1, 9999)
        self.assertEqual(v, 262)

    def test_eval_7(self): # written
        v = collatz_eval(9998, 9999)
        self.assertEqual(v, 92)

    def test_eval_8(self): # written
        v = collatz_eval(333, 666)
        self.assertEqual(v, 145)

    # -----
    # print
    # -----

    def test_print(self): # given
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_1(self): # written
        w = StringIO()
        collatz_print(w, 2, 3, 8)
        self.assertEqual(w.getvalue(), "2 3 8\n")

    def test_print_2(self): # written
        w = StringIO()
        collatz_print(w, 1, 9999, 262)
        self.assertEqual(w.getvalue(), "1 9999 262\n")

    def test_print_3(self): # written
        w = StringIO()
        collatz_print(w, 9998, 9999, 92)
        self.assertEqual(w.getvalue(), "9998 9999 92\n")

    def test_print_4(self): # written
        w = StringIO()
        collatz_print(w, 333, 666, 145)
        self.assertEqual(w.getvalue(), "333 666 145\n")

    # -----
    # solve
    # -----

    def test_solve(self): # given 
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
